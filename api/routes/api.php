<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/', function () {
    return '{"result":true, "count":42}';
    //return json_encode("{'hello':'world'}");
    // return view('welcome');
});


Route::get('/products', function () {
    //  return '{"result":true, "count":42}';
    return '{
      "data": [{
         "image": "https://www.brain-effect.com/media/catalog/product/cache/efab3ae692a45a566899fba747993863/h/a/happy_gut-ppp-web-de-20-10-26-1000x1000-pr_coach_white_2_.jpg",
         "productTitle": "Happy Gut",
         "productDescription": "Für ein gutes Bauchgefühl",
         "productPrice": "34,90 €",
         "pricePerWeight": "EUR 38,78 / 100 g",
         "deliveryLink": "https://www.brain-effect.com/versandkosten"
        },
        {
         "image": "https://www.brain-effect.com/media/catalog/product/cache/efab3ae692a45a566899fba747993863/g/u/guard-ppp-web-multi-21-07-13-2000x2000-pr_white.jpg",
         "productTitle": "ESSENTIALS GUARD",
         "productDescription": "Für ein gutes Bauchgefühl",
         "productPrice": "34,90 €",
         "pricePerWeight": "EUR 38,78 / 100 g",
         "deliveryLink": "https://www.brain-effect.com/versandkosten"
        },
{
         "image": "https://www.brain-effect.com/media/catalog/product/cache/efab3ae692a45a566899fba747993863/s/l/sleep_spray-ppp-web-de-21-07-22-1000x1000-pr_coach_white-cherry_1_.jpg",
         "productTitle": "NEU: SLEEP SPRAY SUMMER EDITION",
         "productDescription": "Für ein gutes Bauchgefühl",
         "productPrice": "34,90 €",
         "pricePerWeight": "EUR 38,78 / 100 g",
         "deliveryLink": "https://www.brain-effect.com/versandkosten"
        },
{
         "image": "https://www.brain-effect.com/media/catalog/product/cache/efab3ae692a45a566899fba747993863/m/o/mood-ppp-web-de-20-10-26-1000x1000-pr_coach_white.jpg",
         "productTitle": "Mood",
         "productDescription": "Für ein gutes Bauchgefühl",
         "productPrice": "34,90 €",
         "pricePerWeight": "EUR 38,78 / 100 g",
         "deliveryLink": "https://www.brain-effect.com/versandkosten"
        }]
      }';
});

Route::get('/product/{id}', function ($id) {
    if ($id == "happy-gut") {
        return '{
          "data": [{
             "image": [{"title": "test", "image": "https://www.brain-effect.com/media/catalog/product/cache/3e69792ac05add87b1e9feecf08ddd92/s/t/stress_less-ppp-web-de-20-10-26-1000x1000-pr_coach_grey_2_.jpg"}, {"title": "test", "image": "https://www.brain-effect.com/media/catalog/product/cache/3e69792ac05add87b1e9feecf08ddd92/h/a/happy_gut-ppp-web-de-20-10-26-1000x1000-ls_1_1.jpg"}, {"title": "test", "image": "https://www.brain-effect.com/media/catalog/product/cache/3e69792ac05add87b1e9feecf08ddd92/h/a/happy_gut-ppp-web-de-20-10-26-1000x1000-ls_1_1.jpg"}, {"title": "test", "image": "https://www.brain-effect.com/media/catalog/product/cache/3e69792ac05add87b1e9feecf08ddd92/h/a/happy_gut-ppp-web-de-20-12-09-1000x1000-ls_3_1_.jpg"}, {"title": "test", "image": "https://www.brain-effect.com/media/catalog/product/cache/3e69792ac05add87b1e9feecf08ddd92/h/a/happy_gut-ppp-web-de-20-12-09-1000x1000-ls_2_1_.jpg"}],
             "productTitle": "Happy Gut",
             "productDescription": "<p class=\"fw-700\">Für ein gutes Bauchgefühl</p><p>Fruchtiges Erdbeer-Getränkepulver mit 9 Mrd. Bakterienkulturen</p><p>Vielseitig anwendbar im Porridge, Joghurt oder Smoothie</p><p>Ergänzt durch den löslichen Ballaststoff Nutriose</p>",
             "productPrice": "34,90 €",
             "pricePerWeight": "EUR 38,78 / 100 g",
             "deliveryLink": "https://www.brain-effect.com/versandkosten"
            }]
      }';
    }
    if ($id == "essentials-guard") {
        return '{
          "data": [{
             "image": [{"title": "test", "image": "https://www.brain-effect.com/media/catalog/product/cache/3e69792ac05add87b1e9feecf08ddd92/s/t/stress_less-ppp-web-de-20-10-26-1000x1000-pr_coach_grey_2_.jpg"}, {"title": "test", "image": "https://www.brain-effect.com/media/catalog/product/cache/3e69792ac05add87b1e9feecf08ddd92/h/a/happy_gut-ppp-web-de-20-10-26-1000x1000-ls_1_1.jpg"}, {"title": "test", "image": "https://www.brain-effect.com/media/catalog/product/cache/3e69792ac05add87b1e9feecf08ddd92/h/a/happy_gut-ppp-web-de-20-10-26-1000x1000-ls_1_1.jpg"}, {"title": "test", "image": "https://www.brain-effect.com/media/catalog/product/cache/3e69792ac05add87b1e9feecf08ddd92/h/a/happy_gut-ppp-web-de-20-12-09-1000x1000-ls_3_1_.jpg"}, {"title": "test", "image": "https://www.brain-effect.com/media/catalog/product/cache/3e69792ac05add87b1e9feecf08ddd92/h/a/happy_gut-ppp-web-de-20-12-09-1000x1000-ls_2_1_.jpg"}],
             "productTitle": "Essentials Guard",
             "productDescription": "<p><b>Für ein gutes Bauchgefühl</b></p><br><p>Fruchtiges Erdbeer-Getränkepulver mit 9 Mrd. Bakterienkulturen</p><p>Vielseitig anwendbar im Porridge, Joghurt oder Smoothie</p><p>Ergänzt durch den löslichen Ballaststoff Nutriose</p>",
             "productPrice": "34,90 €",
             "pricePerWeight": "EUR 38,78 / 100 g",
             "deliveryLink": "https://www.brain-effect.com/versandkosten"
            }]
      }';
    }
    if ($id == "neu:-sleep-spray-summer-edition") {
        return '{
          "data": [{
             "image": [{"title": "test", "image": "https://www.brain-effect.com/media/catalog/product/cache/3e69792ac05add87b1e9feecf08ddd92/s/t/stress_less-ppp-web-de-20-10-26-1000x1000-pr_coach_grey_2_.jpg"}, {"title": "test", "image": "https://www.brain-effect.com/media/catalog/product/cache/3e69792ac05add87b1e9feecf08ddd92/h/a/happy_gut-ppp-web-de-20-10-26-1000x1000-ls_1_1.jpg"}, {"title": "test", "image": "https://www.brain-effect.com/media/catalog/product/cache/3e69792ac05add87b1e9feecf08ddd92/h/a/happy_gut-ppp-web-de-20-10-26-1000x1000-ls_1_1.jpg"}, {"title": "test", "image": "https://www.brain-effect.com/media/catalog/product/cache/3e69792ac05add87b1e9feecf08ddd92/h/a/happy_gut-ppp-web-de-20-12-09-1000x1000-ls_3_1_.jpg"}, {"title": "test", "image": "https://www.brain-effect.com/media/catalog/product/cache/3e69792ac05add87b1e9feecf08ddd92/h/a/happy_gut-ppp-web-de-20-12-09-1000x1000-ls_2_1_.jpg"}],
             "productTitle": "Neu: Sleep Spray Summer Edition",
             "productDescription": "<p><b>Für ein gutes Bauchgefühl</b></p><br><p>Fruchtiges Erdbeer-Getränkepulver mit 9 Mrd. Bakterienkulturen</p><p>Vielseitig anwendbar im Porridge, Joghurt oder Smoothie</p><p>Ergänzt durch den löslichen Ballaststoff Nutriose</p>",
             "productPrice": "34,90 €",
             "pricePerWeight": "EUR 38,78 / 100 g",
             "deliveryLink": "https://www.brain-effect.com/versandkosten"
            }]
      }';
    }
    if ($id == "mood") {
        return '{
          "data": [{
             "image": [{"title": "test", "image": "https://www.brain-effect.com/media/catalog/product/cache/3e69792ac05add87b1e9feecf08ddd92/s/t/stress_less-ppp-web-de-20-10-26-1000x1000-pr_coach_grey_2_.jpg"}, {"title": "test", "image": "https://www.brain-effect.com/media/catalog/product/cache/3e69792ac05add87b1e9feecf08ddd92/h/a/happy_gut-ppp-web-de-20-10-26-1000x1000-ls_1_1.jpg"}, {"title": "test", "image": "https://www.brain-effect.com/media/catalog/product/cache/3e69792ac05add87b1e9feecf08ddd92/h/a/happy_gut-ppp-web-de-20-10-26-1000x1000-ls_1_1.jpg"}, {"title": "test", "image": "https://www.brain-effect.com/media/catalog/product/cache/3e69792ac05add87b1e9feecf08ddd92/h/a/happy_gut-ppp-web-de-20-12-09-1000x1000-ls_3_1_.jpg"}, {"title": "test", "image": "https://www.brain-effect.com/media/catalog/product/cache/3e69792ac05add87b1e9feecf08ddd92/h/a/happy_gut-ppp-web-de-20-12-09-1000x1000-ls_2_1_.jpg"}],
             "productTitle": "Mood",
             "productDescription": "<p><b>Für ein gutes Bauchgefühl</b></p><br><p>Fruchtiges Erdbeer-Getränkepulver mit 9 Mrd. Bakterienkulturen</p><p>Vielseitig anwendbar im Porridge, Joghurt oder Smoothie</p><p>Ergänzt durch den löslichen Ballaststoff Nutriose</p>",
             "productPrice": "34,90 €",
             "pricePerWeight": "EUR 38,78 / 100 g",
             "deliveryLink": "https://www.brain-effect.com/versandkosten"
            }]
      }';
    }
});
// image, product title, product description, product price, product price per weight, product-delivery-link
